/*******************************************************************************
 * 
 * Module Name : tchat
 * 
 * Description : To send chat.
 * 
 * Author(s) : Harinath S.
 * 
 * Date : 2012/07/30
 * 
 * Version : 0.0.1
 * 
 ******************************************************************************/

/* Base Module */

var xmpp = require('node-xmpp');
var server = null;

/*
 * Tchat instance params - chat configurations
 * 
 */

var TChat = function(config) {
	if (config) {
		server = new xmpp.Client({
			jid : config.user,
			password : config.password,
			host : "talk.google.com"
		});
		server.addListener('online', function() {
		});
		return this;
	} else {
		return null;
	}
};

/*
 * Sends chat message
 * 
 * 
 */
TChat.prototype.send = function(to, message) {
	
	if (to && message) {
	
			
			server.send(new xmpp.Element('message', {
				to : to,
				type : 'chat'
			}).c('body').t(message));

			//server.end(); //NEED to check if session exist
	
	}
};

module.exports = TChat;
